#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="nordic"
THEME_FOLDER="lm-21.3-xfce-${THEME}"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
#
# ###############################################################

# ###############################################################
#
# FUNZIONI USATE DALLO SCRIPT, NON MODIFICARE!
#
goto() {
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

command_exists() {
    command -v $1 >/dev/null 2>&1
}

checkEnv() {
    ## Controlla se esistono i requisiti
    REQUIREMENTS='curl'
    if ! command_exists ${REQUIREMENTS}; then
        echo ""
        echo -e "${red}  Affinchè lo script possa funzionare, necessita del pacchetto: ${REQUIREMENTS} ${reset}"
        echo ""
        sleep 1
        exit 1
    fi
}

installStarship() {
    if command_exists starship; then
        echo ""
        echo -e "${red}  Starship è già installato! ${reset}"
        echo ""
        sleep 1
        return
    fi

    if ! curl -sS https://starship.rs/install.sh | sh; then
        echo ""
        echo -e "${red}  Qualcosa è andato storto durante l'installazione di starship! ${reset}"
        echo ""
        sleep 1
        exit 1
    fi
}

copyConfig() {
    ## Controlla se esiste il file .bashrc
    OLD_BASHRC="${HOME}/.bashrc"
    if [[ -e ${OLD_BASHRC} ]]; then
        echo -e "${yellow}  Creo una copia della configurazione di bash in ${HOME}/.bashrc.bak ${reset}"
        if ! cp ${OLD_BASHRC} ${HOME}/.bashrc.bak; then
            echo -e "${red}  Impossibile creare una copia del file di configurazione di bash esistente! ${reset}"
            exit 1
        fi
    fi

    if ! grep -q "TEMA NORDIC" ${OLD_BASHRC}; then
        if [ -d ${BASE_PATH}/${THEME_FOLDER}/bash/ ]; then
            echo -e "${yellow}  Aggiunta di alcune istruzioni al file ${HOME}/.bashrc... ${reset}"
            cat ${BASE_PATH}/${THEME_FOLDER}/bash/custom_bash >> ${HOME}/.bashrc
            cp ${BASE_PATH}/${THEME_FOLDER}/bash/starship.toml ${HOME}/.config/
        else
            echo ""
            echo -e "${red}  Mancano alcuni file di configurazione per la bash! ${reset}"
            echo ""
            sleep 1
            exit 1
        fi
    else
       echo ""
       echo -e "${red}  La configurazione Nordic per il terminale di Xfce è stata già installata! ${reset}"
       sleep 1
       exit 1
    fi
}
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi

source $(dirname $0)/color-schemes

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗    ███╗   ██╗ ██████╗ ██████╗ ██████╗ ██╗ ██████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝    ████╗  ██║██╔═══██╗██╔══██╗██╔══██╗██║██╔════╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗      ██╔██╗ ██║██║   ██║██████╔╝██║  ██║██║██║"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝      ██║╚██╗██║██║   ██║██╔══██╗██║  ██║██║██║"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗    ██║ ╚████║╚██████╔╝██║  ██║██████╔╝██║╚██████╗"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝    ╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝"
echo -e "${yellow}${bold}"
echo "  =================================================================================="
echo "   Script per personalizzare il terminale di Xfce con tema ${THEME^^}"
echo "   Scritto da TGY-TUTORIALS il 21/05/2024"
echo "   Versione: 1.0"
echo "   Ultima modifica: 21/05/2024"
echo "  =================================================================================="
echo -e "${reset}"

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo ""
    echo -e "${green}  Termino lo script...a presto! ${reset}"
    echo ""
    exit 1
    ;;
  *)
    echo ""
    echo -e "${red}  Tasto non valido. Per favore premi 's' o 'n'. ${reset}"
    echo ""
    sleep 1
    exit 1
    ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
    mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ -d ${THEME_FOLDER} ];then
    checkEnv
    installStarship
    if copyConfig; then
        echo -e "${green}  Fatto!\n  Riavvia il terminale per vedere le modifiche.\n ${reset}"
    else
        echo -e "${red}  Ops! Qualcosa è andato storto! ${reset}"
    fi
else
  echo ""
  echo -e "${red}  La cartella ${THEME_FOLDER} non esiste!${reset}"
  echo "  Questo script utilizza risorse che sono parte del tema ${THEME^^}."
  echo "  Assicurati di aver prima scaricato le risorse dal repository di"
  echo "  GitLab eseguendo prima lo script install-res.sh, poi esegui"
  echo "  nuovamente questo script. Buona giornata!"
  echo ""
fi
