#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="nordic"
THEME_FOLDER="lm-21.3-xfce-${THEME}"
CONKY_THEME_FOLDER="Botein"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
#
# ###############################################################

# ###############################################################
#
# FUNZIONI USATE DALLO SCRIPT, NON MODIFICARE!
#
goto() {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi
source $(dirname $0)/color-schemes

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗    ███╗   ██╗ ██████╗ ██████╗ ██████╗ ██╗ ██████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝    ████╗  ██║██╔═══██╗██╔══██╗██╔══██╗██║██╔════╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗      ██╔██╗ ██║██║   ██║██████╔╝██║  ██║██║██║"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝      ██║╚██╗██║██║   ██║██╔══██╗██║  ██║██║██║"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗    ██║ ╚████║╚██████╔╝██║  ██║██████╔╝██║╚██████╗"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝    ╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝"
echo -e "${yellow}${bold}"
echo "  =================================================================================="
echo "  Script per sistemare eventuali problemi di permessi di file e"
echo "  sottocartelle per la cartella ${THEME_FOLDER}"
echo "  Scritto da TGY-TUTORIALS il 21/05/2024"
echo "  Versione: 1.0"
echo "  Ultima modifica: 21/05/2024"
echo "  =================================================================================="
echo -e "${reset}"

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

if [ -d ${BASE_PATH} ];then
  cd ${BASE_PATH}

  if [ -d ${THEME_FOLDER} ];then
    find ${THEME_FOLDER} -type d -print0 | xargs -0 chmod -v 755
    find ${THEME_FOLDER} -type f -print0 | xargs -0 chmod -v 644

    chmod 755 ${THEME_FOLDER}/*.sh
    chmod 755 ${THEME_FOLDER}/user-scripts/.scripts/*
    chmod 755 ${THEME_FOLDER}/configs/.config/autostart/*
    chmod 755 ${THEME_FOLDER}/configs/.config/conky/${CONKY_THEME_FOLDER}/start.sh
    chmod 755 ${THEME_FOLDER}/configs/.config/conky/${CONKY_THEME_FOLDER}/scripts/*.sh
    chmod 755 ${THEME_FOLDER}/configs/.local/bin/*
    chmod 755 ${THEME_FOLDER}/configs/.local/share/applications/*
    chmod 755 ${THEME_FOLDER}/macchina/macchina-linux-x86_64
    chmod 755 ${THEME_FOLDER}/configs/.config/rofi/powermenu/powermenu.sh
  else
    echo ""
    echo "  La cartella ${THEME_FOLDER} non esiste!"
    echo ""
    exit 1
  fi
else
  echo ""
  echo "  La cartella ${BASE_PATH} non esiste!"
  echo ""
  exit 1
fi
