12_ Configurazione del tema di Plymouth “cybernetic” nel terminale
------------------------------------------------------------------------
- Tema scaricato dalla pagina:

https://www.gnome-look.org/p/2106821

NOTA BENE: alcuni comandi per installare lm-mac-style sono nello script install-res.sh. Coloro i quali non utilizzeranno lo script install-res.sh, ma vorranno installare manualmente il tema per Plymouth lm-mac-style, dovranno utilizzare anche questi comandi:

$ sudo apt install plymouth-themes fonts-cantarell -y
$ cd ~/Scaricati/lm-21.3-xfce-nordic
$ sudo cp -r plymouth-theme/lm-mac-style/ /usr/share/plymouth/themes/
$ sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/lm-mac-style/lm-mac-style.plymouth 110

Eseguire in un terminale i seguenti comandi aggiuntivi:

$ sudo update-alternatives --config default.plymouth (scegliere tema 2)
$ sudo update-initramfs -u -k all

NOTA: ignorare l’errore:
W: plymouth module (/usr/lib/x86_64-linux-gnu/plymouth//.so) missing, skipping that theme.

Il tema comunque funzionerà. Se utilizzate i drivers proprietari per schede video NVIDIA, solitamente i temi di plymouth non funzionano. Se utilizzate driver video amdgpu o nouveau invece funzionano bene.
------------------------------------------------------------------------
