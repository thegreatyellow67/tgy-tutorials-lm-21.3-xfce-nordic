Installiamo Glava Visualizer
Vedi script: install-glava.sh
------------------------------------------------------------------------
- Sorgenti di glava visualizer da repo GitLab:

Fonte: https://gitlab.com/wild-turtles-publicly-release/glava/glava

Installare le dipendenze:
$ sudo apt install libgl1-mesa-dev libpulse0 libpulse-dev libxext6 libxext-dev libxrender-dev libxcomposite-dev liblua5.3-dev liblua5.3-0 lua-lgi lua-filesystem libobs0 libobs-dev meson build-essential gcc -y

$ sudo ldconfig
$ sudo ccache -c

$ cd ~/Scaricati/

$ git clone https://gitlab.com/wild-turtles-publicly-release/glava/glava.git glava-src-gitlab

$ cd glava-src-gitlab
$ meson build --prefix /usr
$ ninja -C build
$ sudo ninja -C build install
------------------------------------------------------------------------
