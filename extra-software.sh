#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="nordic"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
PROGRESSBAR_SCRIPT="${BASE_PATH}/progress_bar.sh"
#
# ###############################################################
#
# VARIABILI USATE PER INSTALLAZIONE DEI
# PACCHETTI APT E LA BARRA DI PROGRESSO, NON MODIFICARE!
#
# Dichiara una variabile array che contiene i
# pacchetti apt che devono essere installati
declare -a APT_LIST=("cbonsai" "ccal" "cmatrix" "cmatrix-xfont" "figlet" "lolcat" "libpython3.11-dev" "mpv" "pipx" "python3-full" "python3-pip" "python3-rich" "python3-requests" "pv" "toilet" "toilet-fonts" "tty-clock")
declare -i ARRAY_COUNT=${#APT_LIST[@]}
declare -i COUNTER=0
declare -i STEP=$(( 100 / ${ARRAY_COUNT} ))
declare -i TOTAL_PROGRESS=100
declare -i PACKAGE_NUM=0
#
# ###############################################################

# ###############################################################
#
# FUNZIONI USATE DALLO SCRIPT, NON MODIFICARE!
#
goto() {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

apt_install() {
 if [ "${COUNTER}" -lt "${ARRAY_COUNT}" ]; then
   echo ""
   echo "  ======"
   echo "   Sto installando il pacchetto: $((PACKAGE_NUM+=1))/${ARRAY_COUNT} - ${APT_LIST[${COUNTER}]}"
   echo "  ======"
   echo ""
   sleep 3
   sudo nala install ${APT_LIST[${COUNTER}]} -y
   COUNTER=$((COUNTER+1))
 fi
}

progressbar() {
  # Assicurati che la barra di avanzamento venga
  # ripulita quando l'utente preme Ctrl+c
  enable_trapping
  # Crea la barra di progresso
  setup_scroll_area

  for (( PROGRESS=${STEP}; PROGRESS<=${TOTAL_PROGRESS}; PROGRESS+=${STEP} )); do
    apt_install
    draw_progress_bar ${PROGRESS}
  done
  destroy_scroll_area
}
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi

if [ ! -f "${PROGRESSBAR_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/progress_bar.sh &> /dev/null
  chmod +x ${BASE_PATH}/progress_bar.sh
fi

source $(dirname $0)/color-schemes
source $(dirname $0)/progress_bar.sh

echo -e "${blue}"
echo ""
echo "  ██████╗ ██████╗  ██████╗  ██████╗ ██████╗  █████╗ ███╗   ███╗███╗   ███╗██╗"
echo "  ██╔══██╗██╔══██╗██╔═══██╗██╔════╝ ██╔══██╗██╔══██╗████╗ ████║████╗ ████║██║"
echo "  ██████╔╝██████╔╝██║   ██║██║  ███╗██████╔╝███████║██╔████╔██║██╔████╔██║██║"
echo "  ██╔═══╝ ██╔══██╗██║   ██║██║   ██║██╔══██╗██╔══██║██║╚██╔╝██║██║╚██╔╝██║██║"
echo "  ██║     ██║  ██║╚██████╔╝╚██████╔╝██║  ██║██║  ██║██║ ╚═╝ ██║██║ ╚═╝ ██║██║"
echo "  ╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝╚═╝     ╚═╝╚═╝"
echo ""
echo "  ███████╗██╗  ██╗████████╗██████╗  █████╗"
echo "  ██╔════╝╚██╗██╔╝╚══██╔══╝██╔══██╗██╔══██╗"
echo "  █████╗   ╚███╔╝    ██║   ██████╔╝███████║"
echo "  ██╔══╝   ██╔██╗    ██║   ██╔══██╗██╔══██║"
echo "  ███████╗██╔╝ ██╗   ██║   ██║  ██║██║  ██║"
echo "  ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝"
echo -e "${yellow}${bold}"
echo "  ==========================================================================="
echo "  Script per installare programmi extra da"
echo "  utilizzare nel tema Xfce ${THEME^^}"
echo "  Scritto da TGY-TUTORIALS il 21/05/2024"
echo "  Versione: 1.0"
echo "  Ultima modifica: 21/05/2024"
echo "  ==========================================================================="
echo -e "${reset}"
echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

#
# Per la lista dei pacchetti vedi variabile array APT_LIST
#
clear
echo ""
echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
echo "  ================================================================================"
echo "   Pacchetti:"
echo "   cbonsai | ccal | cmatrix | cmatrix-xfont | figlet | lolcat | libpython3.11-dev"
echo "   mpv | pipx | python3-full | python3-pip | python3-rich | python3-requests | pv"
echo "   toilet | toilet-fonts | tty-clock"
echo ""
echo "   ATTENZIONE! ATTENZIONE! ATTENZIONE!"
echo ""
echo "   NON interrompere lo script in fase di installazione dei pacchetti"
echo "   apt, quasi sicuramente si corromperà il database di apt!"
echo "  ================================================================================"
echo "   Aggiungi al file ${HOME}/.bashrc i seguenti alias:"
echo ""
echo '   alias bonsai="cbonsai -li"'
echo '   alias cal="ccal | lolcat"'
echo '   alias clock="tty-clock -s"'
echo '   alias matrix="cmatrix -B -C red"'
echo '   alias pipes="pipes.sh -t 0"'
echo '   alias nfetch="neofetch --cpu_cores physical --cpu_temp C | lolcat"'
echo '   alias radio="pyradio --play"'
echo '   alias meteo="curl wttr.in/pesaro"'
echo "  ================================================================================"
echo ""
read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
#
# Installa nala se mancante
#
if ! location="$(type -p "nala")" || [ -z "nala" ]; then
  echo ""
  echo "  Installo nala per far funzionare questo script..."
  echo "  ================================================="
  sudo apt install -y nala &> /dev/null
  sleep 3
fi
sudo nala update &> /dev/null
progressbar

#
# Installa git se mancante
#
if ! location="$(type -p "git")" || [ -z "git" ]; then
  clear
  echo ""
  echo "  Installo git per far funzionare questo script..."
  echo "  ================================================="
  sudo apt install -y git &> /dev/null
  sleep 3
fi

clear
echo ""
echo "  Installazione dello script pipes.sh..."
echo "  ======================================="
echo "  Fonte:"
echo ""
echo "  https://github.com/pipeseroni/pipes.sh"
echo "  ======================================="
git clone https://github.com/pipeseroni/pipes.sh.git &> /dev/null
sudo cp pipes.sh/pipes.sh /usr/local/bin/ &> /dev/null
sudo chmod +x /usr/local/bin/pipes.sh &> /dev/null
rm -fr pipes.sh/ &> /dev/null
sleep 3

clear
echo ""
echo "  Installazione di PyRadio..."
echo "  ======================================="
echo "  Fonte:"
echo ""
echo "  https://github.com/coderholic/pyradio"
echo "  ======================================="
wget https://raw.githubusercontent.com/coderholic/pyradio/master/pyradio/install.py &> /dev/null
python3 -m pipx ensurepath
source ~/.profile
python3 install.py -i
rm install.py
sleep 3

echo ""
echo "  PER CAMBIARE IL TEMA DI PYRADIO..."
echo "  ================================================="

echo "  Dopo che questo script sarà concluso, chiudi e"
echo "  riapri un nuovo terminale e avvia pyradio"
echo "  con il comando:"
echo ""
echo "  pyradio [ INVIO ]"
echo ""
echo "  puoi cambiare il tema digitando t, poi"
echo "  selezioni il tema desiderato e premi"
echo "  BARRA SPAZIATRICE."
echo ""
echo "  Per ottenere una lista completa delle"
echo "  scorciatoie di tastiera e le funzioni"
echo "  corrispondenti digita ?"
echo "  ================================================="
echo ""
read -n 1 -r -s -p $'  Premi INVIO per terminare...\n'
