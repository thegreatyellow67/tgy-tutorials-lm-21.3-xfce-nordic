#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="nordic"
CURSOR_THEME="nordzy"
ICON_THEME="nordzy"
THEME_FOLDER="lm-21.3-xfce-${THEME}"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
PROGRESSBAR_SCRIPT="${BASE_PATH}/progress_bar.sh"
PANEL_PROFILE="Nordic"
GRUB_THEME="sugar-candy-nordic"
PLYMOUTH_THEME="lm-mac-style"
#
# ###############################################################
#
# VARIABILI USATE PER INSTALLAZIONE DEI
# PACCHETTI APT E LA BARRA DI PROGRESSO, NON MODIFICARE!
#
# Dichiara una variabile array che contiene i
# pacchetti apt che devono essere installati
declare -a APT_LIST=("bat" "brightnessctl" "btop" "cava" "conky-all" "dconf-editor" "ffmpeg" "fonts-powerline" "fzf" "gimp" "git" "gist" "goodvibes" "gpick" "gnome-calendar" "htop" "inkscape" "jq" "libxfce4ui-utils" "nala" "pinentry-gtk2" "playerctl" "rofi" "ruby" "ruby-dbus" "shutter" "vlc" "xclip" "xfce4-clipman-plugin" "xfce4-panel-profiles" "yad" "zenity")

declare -i ARRAY_COUNT=${#APT_LIST[@]}
declare -i COUNTER=0
declare -i STEP=$(( 100 / ${ARRAY_COUNT} ))
declare -i TOTAL_PROGRESS=100
declare -i PACKAGE_NUM=0
#
# ###############################################################

# ###############################################################
#
# FUNZIONI USATE DALLO SCRIPT, NON MODIFICARE!
#
goto() {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

apt_install() {
  if [ "${COUNTER}" -lt "${ARRAY_COUNT}" ]; then
    echo ""
    echo "  ======"
    echo "   Sto installando il pacchetto: $((PACKAGE_NUM+=1))/${ARRAY_COUNT} - ${APT_LIST[${COUNTER}]}"
    echo "  ======"
    sudo apt install ${APT_LIST[${COUNTER}]} -y &> /dev/null
    sleep 1
    COUNTER=$((COUNTER+1))
  fi
}

progressbar() {
  # Assicurati che la barra di avanzamento venga
  # ripulita quando l'utente preme Ctrl+c
  enable_trapping
  # Crea la barra di progresso
  setup_scroll_area

  for (( PROGRESS=${STEP}; PROGRESS<=${TOTAL_PROGRESS}; PROGRESS+=${STEP} )); do
    apt_install
    draw_progress_bar ${PROGRESS}
  done
  destroy_scroll_area
}

conky_devices() {
  WIFI_DEVICE=`nmcli device | grep wifi\ | awk '{print $1}'`
  LAN_DEVICE=`ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}'`
  declare -a CONKY_FOLDER=("Botein")
  declare -a CONKY_FILE=("Botein")
  declare -i FOLDER_ARRAY_COUNT=${#CONKY_FOLDER[@]}
  declare -i FOLDER_COUNTER=0

  for (( FOLDER_COUNTER=0; FOLDER_COUNTER<=${FOLDER_ARRAY_COUNT}; FOLDER_COUNTER+=1 )); do
    # Sostituzione dei dipositivi di rete WiFi o LAN
    if [[ "${WIFI_DEVICE}" != "" ]]; then
      sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/${CONKY_FOLDER[${FOLDER_COUNTER}]}/${CONKY_FILE[${FOLDER_COUNTER}]}.conf &> /dev/null
    fi

    if [[ "${LAN_DEVICE}" != "" ]]; then
      sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/${CONKY_FOLDER[${FOLDER_COUNTER}]}/${CONKY_FILE[${FOLDER_COUNTER}]}.conf &> /dev/null
    fi
  done
}
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi

if [ ! -f "${PROGRESSBAR_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/progress_bar.sh &> /dev/null
  chmod +x ${BASE_PATH}/progress_bar.sh
fi

source $(dirname $0)/color-schemes
source $(dirname $0)/progress_bar.sh

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗    ███╗   ██╗ ██████╗ ██████╗ ██████╗ ██╗ ██████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝    ████╗  ██║██╔═══██╗██╔══██╗██╔══██╗██║██╔════╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗      ██╔██╗ ██║██║   ██║██████╔╝██║  ██║██║██║"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝      ██║╚██╗██║██║   ██║██╔══██╗██║  ██║██║██║"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗    ██║ ╚████║╚██████╔╝██║  ██║██████╔╝██║╚██████╗"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝    ╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝"
echo -e "${yellow}${bold}"
echo "  =================================================================================="
echo "   Script per automatizzare la copia delle"
echo "   risorse per il tema Xfce ${THEME^^}"
echo "   Scritto da TGY-TUTORIALS il 21/05/2024"
echo "   Versione: 1.3"
echo "   Ultima modifica: 23/05/2024"
echo "  =================================================================================="
echo -e "${reset}"

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ ! -d ${THEME_FOLDER} ];then

  echo ""
  echo "  Sto per scaricare le risorse da GitLab, un pò di pazienza..."
  echo ""

  #
  # Installa git se mancante
  #
  if ! location="$(type -p "git")" || [ -z "git" ]; then
    echo "  Installo git per far funzionare questo script..."
    echo ""
    sudo apt install -y git &> /dev/null
  fi

  git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}.git ${THEME_FOLDER}

  cd ${THEME_FOLDER}
  rm -fr .git

  #
  # Per la lista dei pacchetti vedi variabile array APT_LIST
  #
  clear
  echo ""
  echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
  echo "  ============================================================================================="
  echo "   Pacchetti:"
  echo "   bat | brightnessctl | btop | cava | conky-all | dconf-editor | ffmpeg | fonts-powerline"
  echo "   fzf | gimp | git | gist | goodvibes | gpick | gnome-calendar | htop | inkscape | jq"
  echo "   libxfce4ui-utils (xfce4-about) | nala | pinentry-gtk2 | playerctl | rofi | ruby | ruby-dbus"
  echo "   shutter | vlc | xclip | xfce4-clipman-plugin | xfce4-panel-profiles | yad | zenity"
  echo ""
  echo "   ATTENZIONE! ATTENZIONE! ATTENZIONE!"
  echo ""
  echo "   NON interrompere lo script in fase di installazione dei pacchetti"
  echo "   apt, quasi sicuramente si corromperà il database di apt!"
  echo "  ============================================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  progressbar

  #
  # Crea cartelle eventualmente mancanti
  #
  if [ ! -d ~/.fonts ];then
    mkdir -p ~/.fonts
  fi

  if [ ! -d ~/.themes ];then
    mkdir -p ~/.themes
  fi

  if [ ! -d ~/.icons ];then
    mkdir -p ~/.icons
  fi

  if [ ! -d ~/.config/conky ];then
    mkdir -p ~/.config/conky
  fi

  if [ ! -d ~/.config/autostart ];then
    mkdir -p ~/.config/autostart
  fi

  if [ ! -d ~/.local/share/applications ];then
    mkdir -p ~/.local/share/applications
  fi

  if [ ! -d ~/.local/share/Trash/files ];then
    mkdir -p ~/.local/share/Trash/files
  fi

  if [ ! -d /boot/grub/themes ];then
    sudo mkdir -p /boot/grub/themes
  fi

  clear
  echo ""
  echo "  Sostituzione con utente corrente in"
  echo "  alcuni files di configurazione..."
  echo "  ==================================="
  echo ""
  find configs/.config/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs-last/.config/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs/.local/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs/xfce-config-helper/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find user-scripts/.scripts/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +

  clear
  echo ""
  echo "  Importazione della configurazione dei pannelli"
  echo "  tramite xfce4-panel-profiles..."
  echo "  =============================================="
  echo ""
  #
  # Sostituzione con utente corrente nella configurazione
  # per i pannelli da importare con xfce4-panel-profiles
  #
  mkdir -p configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  tar -xf configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2 -C configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  rm configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2
  find configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  cd configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  tar -cvjSf ${BASE_PATH}/${THEME_FOLDER}/configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2 * &>/dev/null
  cd ${BASE_PATH}/${THEME_FOLDER}
  rm -fr configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  #
  # Importazione della configurazione con xfce4-panel-profiles
  #
  /usr/bin/xfce4-panel-profiles load configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2
  sleep 3

  clear
  echo ""
  echo "  Installazione dei caratteri..."
  echo "  ===================================================================================="
  echo "   Caratteri utilizzati in questo tema:"
  echo ""
  echo "   JetBrainsMono Nerd Font:"
  echo "   https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/JetBrainsMono.zip"
  echo "  ===================================================================================="
  echo ""
  cp -r ${THEME}-fonts/* ~/.fonts
  sudo cp -r ${THEME}-fonts/JetBrainsMono/ /usr/share/fonts/truetype/
  fc-cache -fr
  sudo fc-cache -fr
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema GTK ${THEME^^}, un pò di pazienza..."
  echo "  ==========================================================="
  echo "   Fonti:"
  echo ""
  echo "   https://github.com/EliverLara/Nordic"
  echo "   https://www.pling.com/p/1267246/"
  echo "  ==========================================================="
  echo ""
  cp -r ${THEME}-gtk-themes/* ~/.themes
  sudo cp -r ${THEME}-gtk-themes/* /usr/share/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone ${ICON_THEME^^}, un pò di pazienza..."
  echo "  ==========================================================="
  echo "   Fonti:"
  echo ""
  echo "   https://github.com/alvatip/Nordzy-icon"
  echo "   https://www.xfce-look.org/p/1686927"
  echo "  ==========================================================="
  echo ""
  cp -r ${THEME}-icons/* ~/.icons
  /usr/bin/gtk-update-icon-cache --force ~/.icons/Nordzy &> /dev/null
  /usr/bin/gtk-update-icon-cache --force ~/.icons/Nordzy-dark &> /dev/null
  sudo cp -r ${THEME}-icons/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione dei cursori ${CURSOR_THEME^^}, un pò di pazienza..."
  echo "  ==========================================================="
  echo "   Fonte:"
  echo ""
  echo "   https://github.com/alvatip/Nordzy-cursors"
  echo "  ==========================================================="
  echo ""
  sudo cp -r ${THEME}-cursors/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione degli sfondi..."
  echo "  ============================="
  echo ""
  cp -r ${THEME}-backgrounds/* ~/Immagini
  sleep 3

  clear
  echo ""
  echo "  Installazione di icone varie usate dal tema..."
  echo "  ==================================================================="
  echo "   Elenco:"
  echo ""
  echo "   - Icone personalizzate per i plugins del pannello (create da me)"
  echo "   - Icone per le azioni personalizzate di Thunar (create da me)"
  echo "   - Icone mono per il plugin del Meteo:"
  echo "     https://github.com/kevin-hanselman/xfce4-weather-mono-icons"
  echo "   - Icone Flattycakes per GIMP:"
  echo "     https://github.com/Jantcu/flattycakes"
  echo "  ==================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r misc-icons/.icons/* ~/.icons
  sudo cp -r misc-icons/usr/share/xfce4/weather/icons/* /usr/share/xfce4/weather/icons/ 
  sudo cp -r misc-icons/usr/share/gimp/2.0/icons/* /usr/share/gimp/2.0/icons/

  clear
  echo ""
  echo "  Installazione di configurazioni varie e utilità di ripristino..."
  echo "  =================================================================================="
  echo "   Elenco:"
  echo ""
  echo "   - Files per avvio automatico di applicazioni (autostart)"
  echo "   - btop"
  echo "   - htop"
  echo "   - Visualizzatore audio Cava"
  echo "   - Script di Conky (Botein)"
  echo "   - fastfetch (alternativa a neofetch)"
  echo "     https://github.com/fastfetch-cli/fastfetch"
  echo "   - Personalizzazione di GIMP"
  echo "   - Visualizzatore audio Glava"
  echo "   - File personalizzato gtk.css (in ~/.config/gtk-3.0)"
  echo "   - macchina"
  echo "   - neofetch"
  echo "   - radiotray-NG"
  echo "   - Rofi PowerMenu"
  echo "   - Azioni personalizzate di Thunar"
  echo "     https://github.com/cytopia/thunar-custom-actions"
  echo "   - Menu Ulauncher"
  echo "     tema: https://github.com/KiranWells/ulauncher-nord"
  echo "   - Impostazioni di Xfce"
  echo "   - Impostazioni del terminale di Xfce"
  echo "     tema: https://github.com/nordtheme/xfce-terminal"
  echo "   - Utilità per ripristino impostazioni di Xfce"
  echo "     (xfconf-load e xfconf-dump)"
  echo "     https://github.com/felipec/xfce-config-helper"
  echo "   - File weather.json per le impostazioni del meteo di Conky"
  echo "   - Impostazioni dell'editor di testo (xed)"
  echo "     tema: https://github.com/nordtheme/gedit"
  echo "   - Lista stazioni radio per Goodvibes"
  echo "  =================================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r configs/.config/* ~/.config
  cp -r configs/.local/bin/ ~/.local/
  cp -r configs/.local/share/* ~/.local/share/
  cp configs/.cache/weather.json ~/.cache/
  dconf load /org/x/editor/ < configs/xed-settings.conf

  #
  # DA UTILIZZARE SE LO SCRIPT DI CONKY USA I DEVICES WIFI O LAN
  #
  clear
  echo ""
  echo "  Sostituzione del dipositivo di rete WiFi o LAN"
  echo "  nello script di Conky..."
  echo "  ==============================================="
  echo ""
  conky_devices
  sleep 3

  clear
  echo ""
  echo "  Installazione di scripts utente personalizzati..."
  echo "  ================================================="
  echo ""
  cp -r user-scripts/.scripts ~/
  sleep 3

  clear
  echo ""
  echo "  Copia di un avatar generico per l'utente..."
  echo "  ======================================================================"
  echo "   NOTA BENE:"
  echo ""
  echo "   nella cartella ${THEME_FOLDER}/resources/avatars/ puoi"
  echo "   trovare differenti icone avatar per rappresentare il tuo utente."
  echo "   Basta copiare quella di tuo gradimento nella cartella:"
  echo ""
  echo "   ${HOME}"
  echo ""
  echo "   e rinominarla in .face"
  echo "  ======================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r resources/avatars/avatar-male-female.svg ~/.face

  clear
  echo ""
  echo "  Installazione di macchina..."
  echo "  ============================"
  echo ""
  sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
  sudo chmod +x /usr/local/bin/macchina
  sleep 3

  clear
  echo ""
  echo "  Installazione di RadioTray-NG, un pò di pazienza..."
  echo "  ==================================================="
  echo ""
  sudo dpkg -i radiotray-ng/radiotray-ng_0.2.8_ubuntu_22.04_amd64.deb &> /dev/null
  sudo apt install -fy &> /dev/null
  sleep 3

  clear
  echo ""
  echo "  Installazione di Ulauncher, un pò di pazienza..."
  echo "  ================================================"
  echo ""
  sudo dpkg -i ulauncher/ulauncher_5.15.7_all.deb &> /dev/null
  sudo apt install -fy &> /dev/null
  sleep 3

  clear
  echo ""
  echo "  Installazione di fastfetch, un pò di pazienza..."
  echo "  ================================================"
  echo ""
  # Recupera l'ultimo URL di rilascio fastfetch per il file deb linux-amd64
  FASTFETCH_URL=$(curl -s https://api.github.com/repos/fastfetch-cli/fastfetch/releases/latest | grep "browser_download_url.*linux-amd64.deb" | cut -d '"' -f 4)

  # Scarica l'ultimo file deb di fastfetch
  curl -sL $FASTFETCH_URL -o /tmp/fastfetch_latest_amd64.deb &> /dev/null

  # Installa il file deb scaricato con apt
  sudo apt install /tmp/fastfetch_latest_amd64.deb &> /dev/null

  # Cancella il file /tmp/fastfetch_latest_amd64.deb
  rm /tmp/fastfetch_latest_amd64.deb
  sleep 3

  clear
  echo ""
  echo "  Installazione del visualizzatore audio Cavalier, un pò di pazienza..."
  echo "  ====================================================================="
  echo ""
  sudo flatpak install -y flathub org.nickvision.cavalier &> /dev/null
  sleep 3

  clear
  echo ""
  echo "  Installazione dello sfondo per la finestra di accesso..."
  echo "  ========================================================"
  echo ""
  sudo cp -r login-window/ /usr/share/backgrounds
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema Sugar Candy Nordic per Grub..."
  echo "  ========================================================="
  echo ""
  sudo cp -r ${GRUB_THEME}/ /boot/grub/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema ${PLYMOUTH_THEME} per Plymouth..."
  echo "  ========================================================================="
  echo "   Fonte: https://www.gnome-look.org/p/2106821"
  echo ""
  echo "   Nel terminale si dovranno poi eseguire i seguenti comandi:"
  echo ""
  echo "   $ sudo update-alternatives --config default.plymouth (scegliere tema 2)"
  echo "   $ sudo update-initramfs -u -k all"
  echo ""
  echo "   NOTA: se viene visualizzato, ignorare l’errore:"
  echo "   W: plymouth module (/usr/lib/x86_64-linux-gnu/plymouth//.so) missing,"
  echo "   skipping that theme."
  echo ""
  echo "   Il tema comunque funzionerà. Se utilizzate i drivers proprietari per"
  echo "   schede video NVIDIA, solitamente i temi di plymouth non funzionano."
  echo "   Se utilizzate driver video amdgpu o nouveau invece funzionano bene."
  echo "  ========================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  echo "   Sto installando i pacchetti plymouth-themes e fonts-cantarell,"
  echo "   un pò di pazienza..."
  echo ""
  sudo apt install plymouth-themes fonts-cantarell -y &> /dev/null
  sudo cp -r plymouth-theme/* /usr/share/plymouth/themes/
  sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/${PLYMOUTH_THEME}/${PLYMOUTH_THEME}.plymouth 110
  sleep 3
  
  clear
  echo ""
  echo "  Importazione delle impostazioni di Xfce4 tramite xfconf-load..."
  echo "  Fonte: https://github.com/felipec/xfce-config-helper"
  echo "  ================================================================"
  echo "   Per esportare le impostazioni di Xfce4:"
  echo "   xfconf-dump > config.yml"
  echo ""
  echo "   Per importare le impostazioni di Xfce4:"
  echo "   xfconf-load config.yml"
  echo "  ================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  ${HOME}/.local/bin/xfconf-load configs/xfce-config-helper/xfce4-settings.yml &>/dev/null
  sleep 3

  clear
  echo ""
  echo "  Risorse installate con successo!"
  echo "  ================================"
  sleep 3
  echo ""
  echo "  ██████╗ ██╗ █████╗ ██╗   ██╗██╗   ██╗██╗ ██████╗"
  echo "  ██╔══██╗██║██╔══██╗██║   ██║██║   ██║██║██╔═══██╗"
  echo "  ██████╔╝██║███████║██║   ██║██║   ██║██║██║   ██║"
  echo "  ██╔══██╗██║██╔══██║╚██╗ ██╔╝╚██╗ ██╔╝██║██║   ██║"
  echo "  ██║  ██║██║██║  ██║ ╚████╔╝  ╚████╔╝ ██║╚██████╔╝"
  echo "  ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝  ╚═══╝    ╚═══╝  ╚═╝ ╚═════╝"
  echo ""
  echo "  ██████╗ ███████╗██╗         ██████╗  ██████╗"
  echo "  ██╔══██╗██╔════╝██║         ██╔══██╗██╔════╝"
  echo "  ██║  ██║█████╗  ██║         ██████╔╝██║"
  echo "  ██║  ██║██╔══╝  ██║         ██╔═══╝ ██║"
  echo "  ██████╔╝███████╗███████╗    ██║     ╚██████╗    ██╗██╗██╗"
  echo "  ╚═════╝ ╚══════╝╚══════╝    ╚═╝      ╚═════╝    ╚═╝╚═╝╚═╝"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  
  #
  # Prima di riavviare, lo script copia la configurazione
  # del terminale di Xfce, perché se lo faccio durante lo script
  # l'aspetto del terminale cambia in modo sgradevole
  #
  cp -r configs-last/.config/xfce4/* ~/.config/xfce4

  sudo systemctl reboot

else
  echo "  La cartella ${THEME_FOLDER} esiste! se vuoi scaricare"
  echo "  le risorse nuovamente devi cancellare la cartella e"
  echo "  rieseguire questo script in un terminale. Buona giornata!"
  echo ""
fi
