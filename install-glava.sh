#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="nordic"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
PROGRESSBAR_SCRIPT="${BASE_PATH}/progress_bar.sh"
#
# ###############################################################
#
# VARIABILI USATE PER INSTALLAZIONE DEI
# PACCHETTI APT E LA BARRA DI PROGRESSO, NON MODIFICARE!
#
# Dichiara una variabile array che contiene i
# pacchetti apt che devono essere installati
declare -a APT_LIST=("libgl1-mesa-dev" "libpulse0" "libpulse-dev" "libxext6" "libxext-dev" "libxrender-dev" "libxcomposite-dev" "liblua5.3-dev" "liblua5.3-0" "lua-lgi" "lua-filesystem" "libobs0" "libobs-dev" "meson" "build-essential" "gcc" "ccache")

declare -i ARRAY_COUNT=${#APT_LIST[@]}
declare -i COUNTER=0
declare -i STEP=$(( 100 / ${ARRAY_COUNT} ))
declare -i TOTAL_PROGRESS=100
declare -i PACKAGE_NUM=0
#
# ###############################################################

# ###############################################################
#
# FUNZIONI USATE DALLO SCRIPT, NON MODIFICARE!
#
goto() {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

apt_install() {
 if [ "${COUNTER}" -lt "${ARRAY_COUNT}" ]; then
   echo ""
   echo "=============================="
   echo " Sto installando il pacchetto: $((PACKAGE_NUM+=1))/${ARRAY_COUNT} - ${APT_LIST[${COUNTER}]}"
   echo "=============================="
   echo ""
   sleep 3
   sudo nala install ${APT_LIST[${COUNTER}]} -y
   sleep 1
   COUNTER=$((COUNTER+1))
 fi
}

progressbar() {
  # Assicurati che la barra di avanzamento venga
  # ripulita quando l'utente preme Ctrl+c
  enable_trapping
  # Crea la barra di progresso
  setup_scroll_area

  for (( PROGRESS=${STEP}; PROGRESS<=${TOTAL_PROGRESS}; PROGRESS+=${STEP} )); do
    apt_install
    draw_progress_bar ${PROGRESS}
  done
  destroy_scroll_area
}
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi

if [ ! -f "${PROGRESSBAR_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/progress_bar.sh &> /dev/null
  chmod +x ${BASE_PATH}/progress_bar.sh
fi

source $(dirname $0)/color-schemes
source $(dirname $0)/progress_bar.sh

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗    ███╗   ██╗ ██████╗ ██████╗ ██████╗ ██╗ ██████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝    ████╗  ██║██╔═══██╗██╔══██╗██╔══██╗██║██╔════╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗      ██╔██╗ ██║██║   ██║██████╔╝██║  ██║██║██║"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝      ██║╚██╗██║██║   ██║██╔══██╗██║  ██║██║██║"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗    ██║ ╚████║╚██████╔╝██║  ██║██████╔╝██║╚██████╗"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝    ╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝"
echo -e "${yellow}${bold}"
echo "  =================================================================================="
echo "  Script per installare il visualizzatore grafico Glava"
echo "  Scritto da TGY-TUTORIALS il 21/05/2024"
echo "  Versione: 1.0"
echo "  Ultima modifica: 21/05/2024"
echo "  =================================================================================="
echo -e "${reset}"

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

echo ""
echo "  Sto installando alcuni pacchetti per la compilazione di glava, un pò di pazienza..."
echo "  ==================================================================================="
echo "   ATTENZIONE! ATTENZIONE! ATTENZIONE!"
echo ""
echo "   NON interrompere lo script in fase di installazione dei pacchetti"
echo "   apt, quasi sicuramente si corromperà il database di apt!"
echo "  ==================================================================================="
echo ""
read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'

#
# Installa nala se mancante
#
if ! location="$(type -p "nala")" || [ -z "nala" ]; then
  echo "  Installo nala per far funzionare questo script..."
  sudo apt install -y nala &> /dev/null
fi
sudo nala update &> /dev/null
progressbar

sudo ldconfig
sudo ccache -c

#
# Installa git se mancante
#
if ! location="$(type -p "git")" || [ -z "git" ]; then
  echo "  Installo git per far funzionare questo script..."
  sudo apt install -y git &> /dev/null
fi

cd ${BASE_PATH}

if [ ! -d ~/${BASE_PATH}/glava-src-gitlab ];then
  git clone https://gitlab.com/wild-turtles-publicly-release/glava/glava.git glava-src-gitlab
else
  rm -fr ~/${BASE_PATH}/glava-src-gitlab/
  git clone https://gitlab.com/wild-turtles-publicly-release/glava/glava.git glava-src-gitlab
fi

cd ${BASE_PATH}/glava-src-gitlab

echo ""
echo "  Compilo glava, un pò di pazienza..."
echo ""

#meson build --reconfigure --prefix /usr
meson build --prefix /usr
ninja -C build
sudo ninja -C build install

