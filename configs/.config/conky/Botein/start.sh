#!/bin/bash

# This command will close all active conky
killall conky
sleep 2s
		
conky -c $HOME/.config/conky/Botein/Botein.conf &> /dev/null &

exit
