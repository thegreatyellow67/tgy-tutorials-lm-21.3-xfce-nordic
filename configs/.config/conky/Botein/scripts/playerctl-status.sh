#!/bin/bash

PCTL=$(playerctl status)

if [[ ${PCTL} == "" ]]; then
  echo "Nessuna musica"
elif [[ ${PCTL} == "Stopped" ]]; then
  echo "Stop"
elif [[ ${PCTL} == "Paused" ]]; then
  echo "Pausa"
else
  echo "Stai ascoltando"
fi

exit

